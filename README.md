# Logistic regression for analyzing churn

## Description
This mini project uses churn data from a telecom company to identify factors affecting people leaving their products/services.
We have used Logistic regressions to develop a model in order to quantify the insights found.